<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'nottingham-bid');

/** MySQL database username */

define('DB_USER', 'root');

/** MySQL database password */

define('DB_PASSWORD', 'root');

/** MySQL hostname */

define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'fqeabuv7maaukab1mnbousqkwesjo2hahloazvh88mwhk9jlfsrwiwhtutvaiwc5');
define('SECURE_AUTH_KEY',  'vybnhtbrvaqppnuefhhyqtoj8szorigfs6dbgoisx1ygmm4oduftelbmhwpnleuy');
define('LOGGED_IN_KEY',    '4fwkz1ukjzdzf72gtwpjcqpde3pvk2efvg3tshu18phk5lpspf5xeudbjbt1w6e1');
define('NONCE_KEY',        'mgtdveoznsjtdzzkaug6ma8zagjcvnh65ckzuyrgxetb2ofym5to6qthsq4lznb9');
define('AUTH_SALT',        'njbt48qwaxssdybgvalrwrjhba68afr9lhjwbtmjtw9brax6mrvhh3kjrk3hbp4v');
define('SECURE_AUTH_SALT', 'jgwthkq8xvl9jl24vmibhivtxpqefmdbfcr5uwixnzmte9sjl4exauohn7cu9dru');
define('LOGGED_IN_SALT',   'ao0zrgyoilmrouvsnrxayigjz1lyvr6z9ll9iablrnq7cttryahnaawxboacoag3');
define('NONCE_SALT',       'ty3aupschtwa9fqxifk4u6gpwdiujq8tavr8rvcnwswzpepsv5mh78f5byti8u5d');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress.  A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define ('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
