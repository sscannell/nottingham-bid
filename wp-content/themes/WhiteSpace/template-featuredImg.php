<?php /* Template Name: Featured Page Image + No Title */ get_header(); ?>
	
	<!-- section -->
	<section role="main">
		
	<?php if (have_posts()): while (have_posts()) : the_post(); ?>
	
		<!-- article -->
		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

            <div class="row-fluid">
            	<div class="span12">
			<?php if (has_post_thumbnail()) { ?>
				<div style="text-align:center;"><?php the_post_thumbnail(); ?> </div>
			<?php } ?>
               </div>
            </div>

		<div class="row-fluid clearfix">

			<?php the_content(); ?>

		</div>	
		</article>
		<!-- /article -->
		
	<?php endwhile; ?>
	
	<?php else: ?>
	
		<!-- article -->
		<article>
			
			<h2><?php _e( 'Sorry, nothing to display.', 'WhiteSpace' ); ?></h2>
			
		</article>
		<!-- /article -->
	
	<?php endif; ?>
	
	</section>
	<!-- /section -->

<?php get_footer(); ?>