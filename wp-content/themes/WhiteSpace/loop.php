<?php if (have_posts()): while (have_posts()) : the_post(); ?>
	
	<!-- article -->
	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>



<div class="media borderBottom stdpadb">
						<div class="pull-left">
							<div class="dateCircle">
                            <p class="dateBig"><?php the_field("event_day"); ?></p>
                            <p class="monthSmall"><?php the_field("event_month"); ?> <?php the_field("event_year"); ?></p>
                         </div>
						</div>
		<!-- post thumbnail -->
		<?php if ( has_post_thumbnail()) : // Check if thumbnail exists ?>
			<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" class="pull-left">
				<?php the_post_thumbnail(array(280,188)); // Declare pixel size you need inside the array ?>
			</a>
		<?php endif; ?>
		<!-- /post thumbnail -->
						<div class="media-body">
							<p class="eventTitle"><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></p>
							<em class="teal">Posted on <?php the_time('F jS, Y'); ?></em> <br />
							<br />
						</div>
						   <?php WhiteSpace_excerpt('WhiteSpace_index'); // Build your custom callback length in functions.php ?>
						   
                        <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" class="buttonLink"><span class="iconPlusWhite"></span>Find Out More</a>

					</div>		
		
		<?php edit_post_link(); ?>
	
	</article>
	<!-- /article -->
	
<?php endwhile; ?>

<?php else: ?>

	<!-- article -->
	<article>
		<h2><?php _e( 'Sorry, nothing to display.', 'WhiteSpace' ); ?></h2>
	</article>
	<!-- /article -->

<?php endif; ?>