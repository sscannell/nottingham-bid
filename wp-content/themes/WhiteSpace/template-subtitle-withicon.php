<?php /* Template Name: Sub-Section Page - With Icon */ get_header(); ?>
	
	<!-- section -->
	<section role="main">
		
	<?php if (have_posts()): while (have_posts()) : the_post(); ?>
	
		<!-- article -->
		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

            <div class="row-fluid">
<div class="stdpadt stdpadl stdpadr clearfix">

<div class="span6">
                <div class="floatLeft">
                        <?php if( class_exists( 'kdMultipleFeaturedImages' ) ) {
			    kd_mfi_the_featured_image( 'page-icon-1', 'page' );
			} ?>
                 </div>
                 <div class="stdmarl floatLeft">
                  	<?php the_subtitle(); ?><br />
                  	<h1><?php the_title(); ?></h1>
                </div>
                <div style="clear:left;" class="stdpadt span8">
                  	<?php the_field("intro_paragraph"); ?>
                </div>
                </div>
                <div class="span6">
                	<?php if( class_exists( 'kdMultipleFeaturedImages' ) ) {
			    kd_mfi_the_featured_image( 'vision-image-quote', 'page' );
			} ?>
                </div>

			

               </div>
            </div>

<?php if(get_field('carousel_shortcode')): ?>
<div class="stdpadt clearfix">
<?php the_field("carousel_shortcode"); ?>
</div>
 <?php endif; ?>

		<div class="row-fluid">
		<div class="stdpad clearfix">
			<?php the_content(); ?>
		</div>	
		</div>	
		</article>
		<!-- /article -->
		
	<?php endwhile; ?>
	
	<?php else: ?>
	
		<!-- article -->
		<article>
			
			<h2><?php _e( 'Sorry, nothing to display.', 'WhiteSpace' ); ?></h2>
			
		</article>
		<!-- /article -->
	
	<?php endif; ?>
	
	</section>
	<!-- /section -->

<?php get_footer(); ?>