<?php get_header(); ?>
	
	<!-- section -->
	<section role="main">
	
	<?php if (have_posts()): while (have_posts()) : the_post(); ?>
	
		<!-- article -->
		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

<div class="row-fluid">
            	<div class="stdpadt stdpadl stdpadr clearfix">
<h1>Bid Events</h1>

                <div class="span12">
                	<div class="pull-left">
                        <div class="dateCircle">
                            <p class="dateBig"><?php the_field("event_day"); ?></p>
                            <p class="monthSmall"><?php the_field("event_month"); ?> <?php the_field("event_year"); ?></p>
                         </div>
                  </div>
                  <div class="stdmarl pull-left">
                  	<h1 style="color:#444;font-size:1.8em;text-transform:uppercase;margin-top:0.85em;" class="span8"><?php the_title(); ?></h1>
                </div>

		


                </div>
                
               </div>
            </div>

			<!-- /post title -->
	  <div class="row-fluid">
            	<div class="stdpad clearfix">	
			<!-- post details
			<span class="date"><?php _e( 'Published on', 'WhiteSpace' ); ?> <?php the_time('F j, Y'); ?> <?php the_time('g:i a'); ?></span> -->
			<!-- /post details -->


			<div class="stdmarl pull-right"> <!-- post thumbnail -->
		<?php if ( has_post_thumbnail()) : // Check if thumbnail exists ?>
			<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" class="pull-left">
				<?php the_post_thumbnail(array(280,188)); // Declare pixel size you need inside the array ?>
			</a>
		<?php endif; ?>
		<!-- /post thumbnail --> </div>


			<?php the_content(); // Dynamic Content ?>
			
			<?php // the_tags( __( 'Tags: ', 'WhiteSpace' ), ', ', '<br>'); // Separated by commas with a line break at the end ?>
			
			<p><?php // _e( 'Categorised in: ', 'WhiteSpace' ); the_category(', '); // Separated by commas ?></p>
			
			<p><?php // _e( 'This post was written by ', 'WhiteSpace' ); the_author(); ?></p>
			
			<?php edit_post_link(); // Always handy to have Edit Post Links available ?>
			
			<?php // comments_template(); ?>
               </div>
            </div>			
		</article>
		<!-- /article -->
		
	<?php endwhile; ?>
	
	<?php else: ?>
	
		<!-- article -->
		<article>
			
			<h1><?php _e( 'Sorry, nothing to display.', 'WhiteSpace' ); ?></h1>
			
		</article>
		<!-- /article -->
	
	<?php endif; ?>
	
	</section>
	<!-- /section -->
	
<?php // get_sidebar(); ?>

<?php get_footer(); ?>