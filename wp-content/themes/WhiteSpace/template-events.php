<?php /* Template Name: BID Events Listing inc. Loop */ get_header(); ?>
	
	<?php get_header(); ?>
<?php query_posts('post_type=post&post_status=publish&posts_per_page=10&paged='. get_query_var('paged')); ?>

	<div class="header">	
	<!-- section -->
	<section role="main">
<div class="row-fluid">
				<div class="stdmart stdpadl stdpadr">
					<div class="borderBottom stdpadb clearfix">
						<div class="span12 alignCenter">
			<h1><?php the_title(); ?></h1>
                          <?php the_subtitle(); ?>
						</div>
					</div>
				</div>
			</div>
 <div class="row-fluid">
            	<div class="stdpad">
				<div class="borderBottom stdpadb clearfix">
                <div class="span12">	

	<?php if( have_posts() ): ?>

        <?php while( have_posts() ): the_post(); ?>


	    <div id="post-<?php get_the_ID(); ?>" <?php post_class(); ?>>
<div class="media borderBottom stdpadb stdpadt">
                         <div class="pull-left">
					<div class="dateCircle">
                            <p class="dateBig"><?php the_field("event_day"); ?></p>
                            <p class="monthSmall"><?php the_field("event_month"); ?> <?php the_field("event_year"); ?></p>
                         		</div>
			</div>   

		<!-- post thumbnail -->
		<?php if ( has_post_thumbnail()) : // Check if thumbnail exists ?>
			<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" class="pull-left">
				<?php the_post_thumbnail(array(280,188)); // Declare pixel size you need inside the array ?>
			</a>
		<?php endif; ?>
		<!-- /post thumbnail -->


						<div class="media-body">
							<p class="eventTitle"><?php the_title(); ?> <!-- <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a> --></p>
							<!-- <em class="teal">Posted on <?php the_time('F jS, Y'); ?></em> <br /> -->
							<br />
						</div>

						   <?php WhiteSpace_excerpt('WhiteSpace_index'); // Build your custom callback length in functions.php ?>
						   
                        <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" class="buttonLink"><span class="iconPlusWhite"></span>Find Out More</a>
</div>
            </div><!-- /#post-<?php get_the_ID(); ?> -->

        <?php endwhile; ?>

		<div class="navigation">
			<span class="newer"><?php previous_posts_link(__('« Newer','WhiteSpace')) ?></span> <span class="older"><?php next_posts_link(__('Older »','WhiteSpace')) ?></span>
		</div><!-- /.navigation -->

	<?php else: ?>

		<div id="post-404" class="noposts">

		    <p><?php _e('None found.','WhiteSpace'); ?></p>

	    </div><!-- /#post-404 -->

	<?php endif; wp_reset_query(); ?>

</div></div></div></div>
		
	</section>
	<!-- /section -->
	</div>
	
<?php // get_sidebar(); ?>

<?php get_footer(); ?>