<?php /* Template Name: Two Columns + Single Column */ get_header(); ?>
	
	<!-- section -->
	<section role="main">
		
	<?php if (have_posts()): while (have_posts()) : the_post(); ?>
	
		<!-- article -->
		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<div class="row-fluid">
            	<div class="stdpadt stdpadl stdpadr clearfix">
                <div class="span6">
                  	<?php the_subtitle(); ?><br />
                  	<h1><?php the_title(); ?></h1>
                    
                    <?php the_content(); ?>
                </div>
                <div class="span6">
                	<?php the_field("right_column_content"); ?>
                </div>
              </div>
            </div>   
            

<?php if(get_field('single_column_content')): ?>
            <div class="stdpad">
               	    	<?php the_field("single_column_content"); ?>
            </div> 
 <?php endif; ?> 
 
		</article>
		<!-- /article -->
		
	<?php endwhile; ?>
	
	<?php else: ?>
	
		<!-- article -->
		<article>
			
			<h2><?php _e( 'Sorry, nothing to display.', 'WhiteSpace' ); ?></h2>
			
		</article>
		<!-- /article -->
	
	<?php endif; ?>
	
	</section>
	<!-- /section -->

<?php get_footer(); ?>