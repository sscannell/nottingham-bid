<?php /* Template Name: Sub-Section Page - No Icon */ get_header(); ?>
	
	<!-- section -->
	<section role="main">
		
	<?php if (have_posts()): while (have_posts()) : the_post(); ?>
	
		<!-- article -->
		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

			<div class="row-fluid">
				<div class="stdpadb stdpadl stdpadr stdpadt">
					<div class="borderBottom clearfix">
						<div class="span12">
						<?php if(the_subtitle() != ''): ?>
						  <?php the_subtitle(); ?><br />
						<?php endif; ?> 
						  <h1><?php the_title(); ?></h1>
						</div>
					</div>
				</div>
			</div>

		<div class="row-fluid clearfix">
		<div class="stdpadl stdpadr">

			<?php the_content(); ?>

		</div>	
		</div>	
		</article>
		<!-- /article -->
		
	<?php endwhile; ?>
	
	<?php else: ?>
	
		<!-- article -->
		<article>
			
			<h2><?php _e( 'Sorry, nothing to display.', 'WhiteSpace' ); ?></h2>
			
		</article>
		<!-- /article -->
	
	<?php endif; ?>
	
	</section>
	<!-- /section -->

<?php get_footer(); ?>