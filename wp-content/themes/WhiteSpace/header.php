<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
	<head>
		<meta charset="<?php bloginfo('charset'); ?>">
		<title><?php wp_title(''); ?><?php if(wp_title('', false)) { echo ' :'; } ?> <?php bloginfo('name'); ?></title>
		
		<!-- dns prefetch -->
		<link href="//www.google-analytics.com" rel="dns-prefetch">
		
		<!-- meta -->
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width,initial-scale=1.0">
		<meta name="description" content="<?php bloginfo('description'); ?>">
		<meta name="base" href="<?php bloginfo('wpurl'); ?>/">
		
		<!-- icons -->
		<link href="<?php echo get_template_directory_uri(); ?>/img/icons/favicon.ico" rel="shortcut icon">
		<link href="<?php echo get_template_directory_uri(); ?>/img/icons/touch.png" rel="apple-touch-icon-precomposed">

		<!-- wp css + wpjavascript -->
		<?php wp_head(); ?>

		<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
		<!-- Add Canvas Highlighter -->
		<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.highlight.min.js"></script>
		<script type="text/javascript">$(function() {
			$('.image-map').highlight();

 var url = window.location;
    // Will only work if string in href matches with location
        $('ul.nav a[href="' + url + '"]').parent().addClass('active');

    // Will also work for relative and absolute hrefs
        $('ul.nav a').filter(function () {
            return this.href == url;
        }).parent().addClass('active').parent().parent().addClass('active');

		});</script>

        	<!-- Bootstrap -->
        	<link href="<?php echo get_template_directory_uri(); ?>/css/bootstrap.css" rel="stylesheet" media="screen">
        	<link href="<?php echo get_template_directory_uri(); ?>/css/bootstrap-responsive.css" rel="stylesheet" media="screen">
        	<link href="<?php echo get_template_directory_uri(); ?>/css/style.css" rel="stylesheet" media="screen">
        	<link href='http://fonts.googleapis.com/css?family=PT+Sans:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
			

		<script>
		!function(){
			// configure legacy, retina, touch requirements @ conditionizr.com
			conditionizr()
		}()
		</script>
	</head>
	<body <?php body_class(); ?>>
	
		<!-- wrapper -->
    <div class="container-fluid">
        <div class="header">
			<!-- header -->
			<header class="clear" role="banner">

					<!-- logo -->
            		<div class="row-fluid stdmart">
               		 <div class="span4">
                		</div>
                		<div id="logo" class="span4 alignCenter">
                    		<a href="http://localhost/IPSWICH-CENTRAL-WEB-COPY">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/logo.png" /></a>
                		</div>
                		<div class="span4">
                		</div>
            		</div>
					<!-- /logo -->
			</header>
			<!-- /header -->
					
					<!-- nav -->
					<nav class="nav" role="navigation">
            				<div class="navbar">
            				<div class="navbar-inner">
            				<div class="container">
                         
            				<!-- .btn-navbar is used as the toggle for collapsed navbar content -->
            				<a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
            				<span class="icon-bar"></span>
            				<span class="icon-bar"></span>
            				<span class="icon-bar"></span>
            				</a>
           				 <!-- Be sure to leave the brand out there if you want it shown-->
            				<ul class="nav">
            					<li<?php if (is_front_page()) { echo " class=\"active\""; }?>><a href="http://localhost/IPSWICH-CENTRAL-WEB-COPY/" title="Nottingham BID Homepage">HOME</a></li>
            				</ul>
            				<!-- Everything you want hidden at 940px or less, place within here -->
            				<div class="nav-collapse collapse">
            				    <ul class="nav">
            				        <?php if (function_exists('getNavMenu')): ?>
            				            <?php echo getNavMenu('TopNav','hover'); ?>
            				        <?php endif; ?>
            				          </ul>
            				        </li>
            				    </ul>
            				<!-- .nav, .navbar-search, .navbar-form, etc -->
            				</div>

            				</div>
            				</div>
            				</div>  
					</nav>
					<!-- /nav -->
