        </div>
    </div>
		<!-- /wrapper -->
			<!-- footer -->
			<footer class="footer" role="contentinfo">

    <div id="footer" class="container-fluid">
        <div class="row-fluid">
            <div class="stdpad clearfix">
                <div class="span3 alignLeft">
                    <img src="<?php echo get_template_directory_uri(); ?>/images/logo-ipswichcentral.png" />
              </div>
              <div class="span7 clearfix clear">
                <ul class="footerNav clearfix">
                       <?php if (function_exists('getNavMenu')): ?>
                        <?php echo getNavMenu('FootNav'); ?>
                    <?php endif; ?>
                </ul>
                <p class="copyright clear" style="clear:both;">
                    Copyright &copy; Nottingham BID, 2013. All Rights Reserved.
                </p>
              </div>
              <div class="span2">
                <a class="pull-right" target="_blank" href="http://www.jacobbailey.com"><img src="<?php echo get_template_directory_uri(); ?>/images/logo-jacobbailey.png" /></a>
              </div>      
            </div>
        </div>
    </div>
			</footer>
			<!-- /footer -->

		<?php wp_footer(); ?>
		<!-- scripts -->
   		<script src="<?php echo get_template_directory_uri(); ?>/js/bootstrap.min.js"></script>
    		<script src="<?php echo get_template_directory_uri(); ?>/js/bootstrap-collapse.js"></script>
    		<script src="<?php echo get_template_directory_uri(); ?>/js/bootstrap-dropdown.js"></script>

<!-- analytics -->
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-43771840-1']);
  _gaq.push(['_trackPageview']);
  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
</script>
	
	</body>
</html>